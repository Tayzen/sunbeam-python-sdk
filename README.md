# Sunbeam Python SDK

**The SDK is made for sunbeam v1 and doesn't work on the new v2 API. I'm not sure to update it one day because the v2 API is way simpler and the usage of a Python library is a kind of antipattern while using sunbeam.**

Sunbeam Python SDK is a framework designed to easily build your Sunbeam extensions in Python. It handles all the hassle of building your manifests and parsing the inputs for you. You just need to focus on your core logic, and you're ready to go!

## Example

The library is heavily inspired by the work of tiangolo, the developer of fastAPI and typer, as you can see in the following example:

```python
from sunbeam import Extension, Text, Number, CommandModes

ext = Extension("ext", "an extension for test purpose")

@ext.command(mode=CommandModes.SEARCH, exit=False)
def feature1(param1: str = Text("blabla", True), param2: int = Number(1, False)):
    print(param1, param2)

ext()
```

The resulted manifest will be:

```json
{
    "title": "ext",
    "description": "an extension for test purpose",
    "preferences": [],
    "commands": [
        {
            "title": "Feature1",
            "name": "feature1",
            "mode": "search",
            "params": [
                {
                    "title": "Param1",
                    "name": "param1",
                    "type": "text",
                    "optional": true,
                    "default": "blabla"
                },
                {
                    "title": "Param2",
                    "name": "param2",
                    "type": "number",
                    "optional": false,
                    "default": 1
                }
            ],
            "exit": false
        }
    ]
}
```
