from dataclasses import dataclass
from typing import List, Union, Optional, Callable, Dict
from inspect import signature, Parameter
from sunbeam.manifest import Command, Input, CommandModes, InputTypes
from sunbeam.inputs import InteractiveInput

# TODO: exec command


def format_title(name: str) -> str:
    return name.capitalize().replace("_", " ")


def _infer_input_properties(value):  # TODO: also use python type to infer
    if isinstance(value, float):
        return (InputTypes.NUMBER, value, False)
    elif isinstance(value, bool):
        return (InputTypes.CHECKBOX, value, False)
    elif isinstance(value, str) and "\n" in value:
        return (InputTypes.TEXTAREA, value, False)
    return (InputTypes.TEXT, str(value), False)


@dataclass
class InputCommand:
    name: str
    input_type: InputTypes
    python_type: Optional[type]
    default: Union[str, float, bool]
    optional: bool = True

    @classmethod
    def parse_parameter(cls, param: Parameter):
        name = param.name
        python_type = param.annotation
        input_def = param.default
        if isinstance(input_def, InteractiveInput):
            (input_type, default, optional) = input_def.to_tuple()
        else:
            (input_type, default, optional) = _infer_input_properties(input_def)

        return cls(name, input_type, python_type, default, optional)

    def to_input(self) -> Input:
        return Input(
            format_title(self.name),
            self.name,
            self.input_type,
            self.optional,
            self.default,
        )


@dataclass
class CommandFunction:
    fun: callable
    inputs: List[InputCommand]
    name: str
    title: str

    def execute(self):
        # TODO: execute
        print("Not implemented yet")
        return

    @classmethod
    def parse_function(cls, func: Callable):
        new_cmd = cls.empty()

        new_cmd.name = func.__name__
        new_cmd.title = format_title(new_cmd.name)

        sign = signature(func)
        for _, parameter in sign.parameters.items():
            new_cmd.inputs.append(InputCommand.parse_parameter(parameter))

        return new_cmd

    def to_command(self, mode: CommandModes, exit: bool = False) -> Command:
        return Command(
            self.title, self.name, mode, [inp.to_input() for inp in self.inputs], exit
        )

    @classmethod
    def empty(cls):
        return cls(lambda x: x, [], "", "")


@dataclass
class CommandsContainer:
    commands: Dict[str, CommandFunction]

    def add_function(
        self,
        func: Callable,
        mode: CommandModes = CommandModes.SEARCH,
        exit: bool = False,
    ) -> Command:
        new_cmd = CommandFunction.parse_function(func)
        self.commands[new_cmd.name] = new_cmd
        return new_cmd.to_command(mode, exit)

    def execute(self, payload):  # TODO: class payload
        preferences = payload["preferences"]
        params = payload["params"]
        selected_command = payload["command"]

        command = self.commands.get(selected_command)
        return command.execute({**params, **preferences})

    @classmethod
    def empty(cls):
        return cls({})
