from strenum import (
    StrEnum,
)  # StrEnum is a default type in version 3.11+ the is here for compatibility purpose


class MyStrEnum(StrEnum):
    def __repr__(self) -> str:
        return f"'{self.value}'".lower()
