from dataclasses import dataclass
from sunbeam.manifest import InputTypes
from typing import Union, Tuple

@dataclass
class InteractiveInput: # TODO: deafult should be optional
    type_: InputTypes
    default: Union[str, float, bool]
    optional: bool = True

    def to_tuple(self) -> Tuple[InputTypes, Union[str, float, bool], bool]:
        return (self.type_, self.default, self.optional)

class Text(InteractiveInput):
    def __init__(self, default: Union[str, float, bool], optional: bool = True):
        super().__init__(InputTypes.TEXT, default, optional)

class TextArea(InteractiveInput):
    def __init__(self, default: Union[str, float, bool], optional: bool = True):
        super().__init__(InputTypes.TEXTAREA, default, optional)

class Password(InteractiveInput):
    def __init__(self, default: Union[str, float, bool], optional: bool = True):
        super().__init__(InputTypes.PASSWORD, default, optional)

class Checkbox(InteractiveInput):
    def __init__(self, default: Union[str, float, bool], optional: bool = True):
        super().__init__(InputTypes.CHECKBOX, default, optional)

class Number(InteractiveInput):
    def __init__(self, default: Union[str, float, bool], optional: bool = True):
        super().__init__(InputTypes.NUMBER, default, optional)
