from dataclasses import asdict
from sunbeam.manifest import Manifest, CommandModes
from sunbeam.command import CommandsContainer
import sys
import json
from typing import Any, Callable


class Extension:
    """Sunbeam extension with manifest, commands and execution"""

    def __init__(
        self,
        title: str,
        description: str,
        manifest: Manifest = Manifest.empty(),
        commands: CommandsContainer = CommandsContainer.empty(),
    ) -> None:
        self.manifest: Manifest = manifest
        self.title: str = title
        self.description: str = description
        self.commands: CommandsContainer = commands

    @property
    def title(self) -> str:
        return self._title

    @title.setter
    def title(self, title: str):
        self._title = title
        self.manifest.title = title

    @property
    def description(self) -> str:
        return self._description

    @description.setter
    def description(self, description: str):
        self._description = description
        self.manifest.description = description

    def command(self, mode: CommandModes, exit: bool = False):
        def inner(func: Callable):
            manifest_cmd = self.commands.add_function(func, mode, exit)
            self.manifest.add_command(manifest_cmd)

        return inner

    def add_preference(self):
        # TODO: code preference
        pass

    def run(self):
        if len(sys.argv) == 1:
            manifest = self.manifest.to_json(4)
            print(manifest)
            sys.exit(0)
        payload = json.loads(sys.argv[1])
        print(self.commands.execute(payload))

    def __repr__(self) -> str:
        return repr(self.manifest)

    def __call__(self, *args: Any, **kwargs: Any) -> Any:
        return self.run(*args, **kwargs)
