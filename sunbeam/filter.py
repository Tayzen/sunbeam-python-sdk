from dataclasses import dataclass
from typing import List, Union
from .utils import MyStrEnum
from enum import auto


class FilterActionTypes(MyStrEnum):
    COPY = auto()
    OPEN = auto()
    EDIT = auto()
    RUN = auto()
    RELOAD = auto()
    EXIT = auto()


@dataclass
class ActionParams:
    default: Union[str, bool, float]
    required: bool


@dataclass
class FilterAction:
    title: str
    key: str
    type: FilterActionTypes


class FilterActionCopy(FilterAction):
    def __init__(self, title: str, key: str, text: str, exit: bool):
        super().__init__(title, key, FilterActionTypes.COPY)
        self.text = text
        self.exit = exit


class FilterActionOpen(FilterAction):
    def __init__(self, title: str, key: str, url: str, path: str):
        super().__init__(title, key, FilterActionTypes.OPEN)
        self.url = url
        self.path = path


class FilterActionEdit(FilterAction):
    def __init__(self, title: str, key: str, path: str, reload: bool, exit: bool):
        super().__init__(title, key, FilterActionTypes.EDIT)
        self.path = path
        self.reload = reload
        self.exit = exit


class FilterActionRun(FilterAction):
    def __init__(
        self,
        title: str,
        key: str,
        command: str,
        params: List[Union[ActionParams, str, bool, float]],
        reload: bool,
        exit: bool,
    ):
        super().__init__(title, key, FilterActionTypes.RUN)
        self.command = command
        self.params = params
        self.reload = reload
        self.exit = exit


class FilterActionReload(FilterAction):
    def __init__(
        self,
        title: str,
        key: str,
        params: List[Union[ActionParams, str, bool, float]],
    ):
        super().__init__(title, key, FilterActionTypes.RELOAD)
        self.params = params


class FilterActionExit(FilterAction):
    def __init__(self, title: str, key: str):
        super().__init__(title, key, FilterActionTypes.EXIT)


@dataclass
class FilterItem:
    title: str
    accessories: List[str]
    actions: List[FilterAction]

    def add_copy_action(self, title: str, key: str, text: str, exit: bool):
        self.actions.append(FilterActionCopy(title, key, text, exit))

    # TODO: other add actions + pass context for run and reload to infer params
