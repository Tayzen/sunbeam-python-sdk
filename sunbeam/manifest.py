from dataclasses import dataclass
from enum import auto
from typing import List, Union
from dataclasses import asdict
import json
from .utils import MyStrEnum


class InputTypes(MyStrEnum):
    TEXT = auto()
    TEXTAREA = auto()
    PASSWORD = auto()
    CHECKBOX = auto()
    NUMBER = auto()


class CommandModes(MyStrEnum):
    SEARCH = auto()
    FILTER = auto()
    DETAIL = auto()
    TTY = auto()
    SILENT = auto()


@dataclass
class Input:
    title: str
    name: str
    type_: InputTypes
    optional: bool
    default: Union[str, float, bool]


@dataclass
class Command:
    title: str
    name: str
    mode: CommandModes
    params: List[Input]
    exit: bool


@dataclass
class Manifest:
    title: str
    description: str
    preferences: List[Input]
    commands: List[Command]

    @classmethod
    def empty(cls):
        return cls("", "", [], [])

    def get_command(self, command_name: str) -> Command:
        command_index = self.commands.index(
            lambda command: command.name == command_name
        )
        return self.commands[command_index]

    def add_command(self, command: Command):
        self.commands.append(command)

    def to_json(self, indent=4):
        return json.dumps(asdict(self), indent=indent).replace("'type_':", "'type':")

    def __repr__(self) -> str:
        return self.to_json(0)
